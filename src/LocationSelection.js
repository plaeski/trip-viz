import React from 'react';
import Tippy from '@tippy.js/react';
import DateCard from './DateCard';

const LocationSelection = ({ dates, addDay, addToDay, reorderDay }) =>  {  
  return (
    <div className="left-rail">
      <div className="card-list">
        {dates.map((day, i) => (
          <DateCard
            index={i}
            locations={day.locations}
            color={day.color}
            addToDay={addToDay}
            reorderDay={reorderDay}
          />
        ))}
      </div>
      <Tippy content="Add Day" placement="left">
        <button className="add-day-btn" onClick={addDay}>+</button>
      </Tippy>
    </div>
  )
};

export default LocationSelection;
