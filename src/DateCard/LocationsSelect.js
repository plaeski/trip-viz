import React from 'react';
import axios from 'axios';
import AsyncSelect from 'react-select/async';

const ACCESS_TOKEN = 'pk.eyJ1IjoibHBsYXNlc2tpIiwiYSI6ImNpd2Yxb3FsMzBjMHgyd2xrZGVucXk5MXoifQ.TuvmEdp2oMfd3ApotTJqRA';

const params = {
  access_token: ACCESS_TOKEN,
  language: 'en',
}

const load0ptions = async (inputValue) => {
  try {
    const resp = await axios.get(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(inputValue)}.json`,
      { params }
    );

    return resp.data.features.map(({ center, matching_place_name, place_name, id }) => ({
      label: matching_place_name || place_name,
      value: center,
      id,
    }));
  } catch (err) {
    return [];
  }
}

const LocationSelect = (props) => (
  <AsyncSelect
    loadOptions={load0ptions}
    cacheOptions
    onChange={props.onChange}
    value={null}
    placeholder="Enter Location Name"
    noOptionsMessage={({ inputValue }) => {
      return inputValue === ''
        ? 'Type a location name to get started'
        : 'Location not found'
    }}
    styles={{
      option: (provided, state) => ({
        ...provided,
        color: state.isFocused ? '#FFF' : provided.color,
      }),
    }}
    theme={theme => ({
      ...theme,
      colors: {
        ...theme.colors,
        neutral0: '#404040',
        neutral20: '#a0a0a0',
        neutral80: '#a0a0a0',
        primary25: 'rgba(176, 20, 85, 0.5)',
      },
    })}
  />
);

export default LocationSelect;
