import React from 'react';
import  { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import DeleteIcon from '@material-ui/icons/Delete';
import LocationSelect from './LocationsSelect';
import './DateCard.scss';

const DateCard = (props) => {

  const onDragEnd = ({ destination, source }) => {
    console.log(destination, source);
    props.reorderDay(props.index, source.index, destination.index)
  }

  return (
    <div className="card date-card">
      <h2>Day {props.index + 1}</h2>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="day-place-list">
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
            > 
              {props.locations.map((place, i) => (
                <Draggable draggableId={place.id} index={i} key={place.id}>
                  {(provided) => (
                    <div
                      className="location-card"
                      ref={provided.innerRef}
                      style={provided.draggableProps.style}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}>
                        <p title={place.label}>{place.label}</p>
                        <button className="icon-btn" onClick={() => {}}><DeleteIcon /></button>
                      </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>                
          )}
        </Droppable>
      </DragDropContext>
      <LocationSelect onChange={(update) => props.addToDay(props.index, update)} />
    </div>
  )
};

export default DateCard;
