import React, { useState } from 'react';
import LocationSelection from './LocationSelection';
import Map from './Map';
import './App.scss';

const defaultDays = [
  {
    locations: [{
      label: 'Tokyo, Tokyo Prefecture, Japan',
      value: [139.77, 35.68],
      id: 'TokyoJapan'
  }, {
      label: 'Yokohama, Kanagawa Prefecture, Japan',
      value: [139.63333, 35.43333],
      id: 'YokohamaJapan'
  }],
    color: [Math.random() * 125, Math.random() * 125, Math.random() * 125],
  }
];

function App() {
  const [dates, updateDays] = useState(defaultDays);

  const addDay = () => updateDays([
    ...dates,
    {
      locations: [],
      color: [Math.random() * 125, Math.random() * 125, Math.random() * 125],
    },
  ]);

  const addToDay = (index, place) => {
    const updatedDays = [...dates];
    updatedDays[index].locations = updatedDays[index].locations.concat(place);
    updateDays(updatedDays);
  }

  const reorderDay = (dateIndex, fromIndex, toIndex) => {
    const updatedLocations = [...dates[dateIndex].locations];
    const removed = updatedLocations.splice(fromIndex, 1);
    updatedLocations.splice(toIndex, 0, removed[0]);

    const updatedDays = [...dates];
    updatedDays[dateIndex].locations = updatedLocations;
    updateDays(updatedDays);
  }

  return (
    <div className="App">
      <LocationSelection
        dates={dates}
        addDay={addDay}
        addToDay={addToDay}
        reorderDay={reorderDay}
      />
      <Map dates={dates} />
    </div>
  );
}

export default App;
