import React, { useState, useMemo } from 'react';
import DeckGL from '@deck.gl/react';
import { ArcLayer } from '@deck.gl/layers';
import { StaticMap } from 'react-map-gl';
import { SizeMe } from 'react-sizeme';

const ACCESS_TOKEN = 'pk.eyJ1IjoibHBsYXNlc2tpIiwiYSI6ImNpd2Yxb3FsMzBjMHgyd2xrZGVucXk5MXoifQ.TuvmEdp2oMfd3ApotTJqRA';

const Map = (props) => {
  const [viewState, updateViewState] = useState({
    longitude: 139.76669,
    latitude: 35.6804,
    zoom: 13,
    pitch: 0,
    bearing: 0
  });

  const arcs = useMemo(() => {
    return props.dates.reduce((list, date, dateIndex) => {
      const arcs = [];
      date.locations.forEach((point, i) => {
        if (point && date.locations[i + 1]) {
          arcs.push([point.value, date.locations[i + 1].value, date.color]);
        }
      });
      return list.concat(arcs);
    }, [])
  }, [props.dates]);

  const arcLayer = new ArcLayer({
    id: 'arc-layer',
    data: arcs,
    pickable: true,
    getWidth: 12,
    getSourcePosition: d => d[0],
    getTargetPosition: d => d[1],
    getSourceColor: d => d[2],
    getTargetColor: d => d[2],
    widthMaxPixelswidthMaxPixels: 3,
  });

  return (
    <div className="map">
      <SizeMe>
        {({ size: { width }}) => (
          <DeckGL
            width={width}
            controller={true}
            viewState={viewState}
            onViewStateChange={({ viewState }) => updateViewState(viewState)}
            layers={[arcLayer]}
          >
            <StaticMap
              mapboxApiAccessToken={ACCESS_TOKEN}
              mapOptions={{
                style: 'mapbox://styles/mapbox/dark-v9',
              }}
            />
          </DeckGL>          
        )}
      </SizeMe>
    </div>
  );
}

export default Map;